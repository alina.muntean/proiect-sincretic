# Proiect Sincretic

## Nume
Traversarea unui arbore binar

## Descriere
-Acest proiect contine un algoritm scris in limbajul C++ care da un arbore binar populat cu elemente aleatorii si il traverseaza in diferite moduri, respectiv: "pre-ordine", "in-ordine", "post-ordine" si "pe nivel". 
-Un arbore binar ordonat este un arbore în care fiecare nod are cel mult doi copii, stângul și dreptul. Cheia unui nod este mai mică decât cheia oricărui nod din subarborele său stâng și mai mare decât cheia oricărui nod din subarborele său drept.
-Programul folosește pointeri și funcții recursive pentru manipularea și traversarea arborelui.

## Utilizare
Se vizualizeaza output-ul programului:
Pre-ordine: 10 5 3 7 15 12 18 
In-ordine: 3 5 7 10 12 15 18 
Post-ordine: 3 7 5 12 18 15 10 
Pe nivel: 10 5 15 3 7 12 18 

## Exemplu de rulare
$ gcc proiect.cpp -o arbore_binar
$ ./arbore_binar

## Mediul de dezvoltare
Limbaj:C++
IDE:Visual Studio Code

## Roadmap
-Imbunatatirea performantei algoritmului
-Adaugarea unei functii de generare a nodurilor
-Imbunatatirea interactiunii cu utilizatorul

## Autor
Acest proiect a fost realizat de Muntean Andreea-Alina.

## Statusul proiectlui
Dezvoltarea proiectului este activă.

## Bibliografie
http://staff.cs.upt.ro/~chirila/teaching/upt/id-aa/lectures/AA-ID-Cap08-2.pdf
https://www.pbinfo.ro/articole/25641/arbori-binari




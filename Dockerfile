
FROM gcc:latest
COPY proiect.cpp /app/
WORKDIR /app
RUN gcc -o proiect proiect.cpp -lstdc++
CMD ["./proiect"]

#include<iostream>
#include<list>

using namespace std;

struct TreeNode{
    int data;
    TreeNode* left;
    TreeNode* right;

    //Constructor explicit
    TreeNode(int value)
    {
        data=value;
        left=nullptr;
        right=nullptr;
    }
};

void preOrderTraversal(TreeNode* root)
{
    if(root!=nullptr)
    {
        cout<<root->data<<" ";
        preOrderTraversal(root->left);
        preOrderTraversal(root->right);
    }
}

void inOrderTraversal(TreeNode* root)
{
    if(root!=nullptr)
    {
        inOrderTraversal(root->left);
        cout<<root->data<<" ";
        inOrderTraversal(root->right);
    }
}

void postOrderTraversal(TreeNode* root)
{
    if(root!=nullptr)
    {
        postOrderTraversal(root->left);
        postOrderTraversal(root->right);
        cout<<root->data<<" ";
    }
}

void levelOrderTraversal(TreeNode* root)
{
    if(root==nullptr)
    {
        return;
    }

    list<TreeNode*>nodeList;
    nodeList.push_back(root);

    while(!nodeList.empty())
    {
        TreeNode* current=nodeList.front();
        nodeList.pop_front();

        cout<<current->data<<" ";

        if(current->left!=nullptr)
        {
            nodeList.push_back(current->left);
        }

        if(current->right!=nullptr)
        {
            nodeList.push_back(current->right);
        }
    }
}

int main()
{
    TreeNode* root = new TreeNode(10);
    root->left=new TreeNode(5);
    root->right=new TreeNode(15);
    root->left->left=new TreeNode(3);
    root->left->right=new TreeNode(7);
    root->right->left=new TreeNode(12);
    root->right->right=new TreeNode(18);

    cout<<"Pre-ordine: ";
    preOrderTraversal(root);
    cout<<"\n";

    cout<<"In-ordine: ";
    inOrderTraversal(root);
    cout<<"\n";

    cout<<"Post-ordine: ";
    postOrderTraversal(root);
    cout<<"\n";

    cout<<"Pe nivel: ";
    levelOrderTraversal(root);
    cout<<"\n";

    return 0;
}